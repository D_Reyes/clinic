# Clinic

Clinic es una pequeña aplicacción de pacientes y diágnosticos desarrollado con Laravel para Mulhacén Soft.

## Tecnología

Clinic está desarrollado entre otros con:

- [Laravel 8](https://www.laravel.com/) - The PHP Framework for Web Artisans
-- [Laravel Jetstream](https://jetstream.laravel.com/)
-- [Laravel Livewire](https://laravel-livewire.com/)
-- [Laravel Sail](https://laravel.com/docs/8.x/sail) - Docker
-- [Laravel Mix](https://laravel-mix.com/) - WebPack
- [Tailwindcss](https://tailwindcss.com/)

## Requisitos

- [Laravel](https://laravel.com/docs/8.x/deployment#server-requirements)
- PHP 8.1
- Mysql (Configuración por defecto)
- SQLite (Configuración por defecto) - Test unitarios

## Instalación

Instalar las dependencias.

```sh
composer install
```

A continuación copiamos el contenido del archivo env.example a un nuevo archivo llamado .env. Ahora podemos configurar los datos de acceso a la base de datos entre otros parametros como el monitor de errores de Bugsnag.

Para entornos de producción...

```sh
composer install --no-dev
```

## Acceso

Puedes acceder mediante los siguientes usuarios:

```sh
email: admin@demo.test
contraseña: admin

email: demo@demo.test
contraseña: demo
```

## Desarrollo

Clinic utiliza seeder y factory para cargar con datos de prueba en la BD.

```sh
php artisan migrate --seed
```

También incluye test unitarios, los lanzamos mediante:

```sh
php artisan test
```

Clinic utiliza Webpack con Laravel Mix para los assets.

Para instalar las dependencias escribirmos en la consola:

```sh
npm install
```

Para generar los assets (css + js):

```sh
npm run dev
```

Para producción:

```sh
npm run prod
```

## Docker

Puedes instalar Clinic mediante docker, es una buena opción si no cumples con los requisitos.

Necesitarás instalar Docker en tu sistema, Docker Desktop en la mejor opción para Windows y Mac, para Linux debemos de instalar Docker Engine y Docker Compose. Para su descarga y más información visita [Docker](https://docs.docker.com/get-docker/)

El siguiente comando utiliza un pequeño contenedor Docker que contiene PHP y Composer para instalar las dependencias de la aplicación:

```sh
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```

Para levantar el contenedor ejecutamos:

```sh
./vendor/bin/sail up
```

Recuerda añadir sail antes de ejecutar cualquier comando en el contenedor, ejemplo:

```sh
./vendor/bin/sail composer install
./vendor/bin/sail php artisan clear:cache
./vendor/bin/sail npm run dev
```

La dirección del servidor web por defecto es:

```sh
0.0.0.0
```

## License

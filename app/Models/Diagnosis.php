<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Wildside\Userstamps\Userstamps;

class Diagnosis extends Model
{
    use HasFactory, LogsActivity, SoftDeletes, Userstamps;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date',
        'description',
        'patient_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime',
        'description' => 'encrypted',
    ];

    /**
     * Scope a query to only include diagnoses by end date.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $query
     * @param string  $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEndDate(Builder $query, $search)
    {
        $search = ('/' == substr($search, 2, 1)) ? Carbon::parse('d/m/Y', $search) : $search;
        return $query->whereDate('date', '<=', $search);
    }

    /**
     * Scope a query to only include diagnoses by patient.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $query
     * @param int|array  $patient_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePatient(Builder $query, $patient_id)
    {
        return $query->whereIn('patient_id', is_array($patient_id) ? $patient_id : explode(',', $patient_id));
    }

    /**
     * Scope a query to only include diagnoses by start date.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $query
     * @param string  $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStartDate(Builder $query, $search)
    {
        $search = ('/' == substr($search, 2, 1)) ? Carbon::parse('d/m/Y', $search) : $search;
        return $query->whereDate('date', '>=', $search);
    }

    /**
     * Activity log options.
     *
     * @return void
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    /**
     * Get the patient that owns the diagnosis.
     *
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
}

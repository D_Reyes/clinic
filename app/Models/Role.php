<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;

class Role extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * ID for admin.
     *
     * @var int
     */
    const ADMIN = 1;

    /**
     * ID for user.
     *
     * @var int
     */
    const USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Activity log options.
     *
     * @return void
     */
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    /**
     * Get the users with a role.
     *
     * @return void
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'max:150',
            ],
            'surname' => [
                'required',
                'string',
                'max:150',
            ],
            'id_card' => [
                'required',
                'string',
                'max:20',
                'spanish_tax_number',
                "unique:App\Models\Patient,id_card,{$this->patient->id},id,deleted_at,NULL",
            ],
            'email' => [
                'required',
                'email',
                'max:250',
            ],
            'phone' => [
                'required',
                'string',
                'max:50',
            ],
            'address' => [
                'nullable',
                'string',
                'max:250',
            ],
            'postcode' => [
                'nullable',
                'string',
                'max:20',
            ],
            'city' => [
                'nullable',
                'string',
                'max:150',
            ],
            'state' => [
                'nullable',
                'string',
                'max:150',
            ],
            'country' => [
                'nullable',
                'string',
                'max:120',
            ],
        ];
    }
}

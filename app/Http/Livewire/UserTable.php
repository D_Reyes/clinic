<?php

namespace App\Http\Livewire;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Laravel\Jetstream\InteractsWithBanner;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;

class UserTable extends DataTableComponent
{
    use AuthorizesRequests, InteractsWithBanner;

    /**
     * Indicates if user deletion is being confirmed.
     *
     * @var bool
     */
    public bool $confirmingDeletion = false;

    /**
     * The current model.
     *
     * @var \App\Models\User
     */
    public $model;

    /**
     * Current modal text.
     *
     * @var string
     */
    public $modalText;

    /**
     * The array defining the columns of the table.
     *
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'name')
                ->sortable(),
            Column::make(__('Role'), 'role_id')
                ->sortable(),
            Column::blank(),
        ];
    }

    /**
     * Define the filters array
     *
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            'role_id' => Filter::make(__('Role'))
                ->select([
                    '' => __('Any'),
                    Role::ADMIN => __('Administrators'),
                    Role::USER => __('Users'),
                ]),
        ];
    }

    /**
     * The base query with search and filters for the table.
     *
     * @return Builder|Relation
     */
    public function query(): Builder
    {
        return User::with('role')
            ->when($this->getFilter('search'), fn ($query, $search) => $query->search($search))
            ->when($this->getFilter('role_id'), fn ($query, $search) => $query->role($search));
    }

    /**
     * The view to render each row of the table.
     *
     * @return string
     */
    public function rowView(): string
    {
        return 'livewire.user.table.rows';
    }

    /**
     * The view to add any modals for the table, could also be used for any non-visible html
     *
     * @return string
     */
    public function modalsView(): string
    {
        return 'livewire.components.modal.delete';
    }

    /**
     * Show delete modal.
     *
     * @param integer $id
     * @return void
     */
    public function confirmDeletion(int $id): void
    {
        $this->confirmingDeletion = true;
        $this->model = User::findOrFail($id);
        $this->modalText = $this->model->name;
    }

    /**
     * Reset delete modal.
     *
     * @return void
     */
    public function resetDeletion(): void
    {
        $this->reset('confirmingDeletion', 'modalText', 'model');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function delete(): void
    {
        $this->authorize('delete', $this->model);

        if (Auth::user()->id === $this->model->id) {
            $this->resetDeletion();
            $this->dangerBanner(__('You can\'t delete yourself.'));
            return;
        }

        $this->model->delete();
        $this->resetDeletion();
        $this->banner(__('The user has been successfully deleted.'));
    }
}

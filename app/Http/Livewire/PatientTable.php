<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Patient;
use Laravel\Jetstream\InteractsWithBanner;

class PatientTable extends DataTableComponent
{
    use InteractsWithBanner;

    /**
     * Indicates if the current user can select columns.
     *
     * @var boolean
     */
    public bool $columnSelect = true;

    /**
     * Remember the selection of columns.
     *
     * @var boolean
     */
    public bool $rememberColumnSelection = false;

    /**
     * Indicates if patient deletion is being confirmed.
     *
     * @var bool
     */
    public bool $confirmingDeletion = false;

    /**
     * The current model.
     *
     * @var \App\Models\Patient
     */
    public $model;

    /**
     * Current modal text.
     *
     * @var string
     */
    public $modalText;

    /**
     * The array defining the columns of the table.
     *
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Name'), 'full_name')
                ->selected()
                ->excludeFromSelectable(),
            Column::make(__('Email'), 'email')
                ->excludeFromSelectable(),
            Column::make(__('Phone'), 'phone')
                ->excludeFromSelectable(),
            Column::make(__('City'), 'city')
                ->excludeFromSelectable(),
            Column::make(__('Country'), 'country'),
            Column::blank()
                ->excludeFromSelectable(),
        ];
    }

    /**
     * The base query with search and filters for the table.
     *
     * @return Builder|Relation
     */
    public function query(): Builder
    {
        return Patient::query()
            ->when($this->getFilter('search'), fn ($query, $search) => $query->search($search));
    }

    /**
     * The view to render each row of the table.
     *
     * @return string
     */
    public function rowView(): string
    {
        return 'livewire.patient.table.rows';
    }

    /**
     * The view to add any modals for the table, could also be used for any non-visible html
     *
     * @return string
     */
    public function modalsView(): string
    {
        return 'livewire.components.modal.delete';
    }

    /**
     * Show delete modal.
     *
     * @param integer $id
     * @return void
     */
    public function confirmDeletion(int $id): void
    {
        $this->confirmingDeletion = true;
        $this->model = Patient::findOrFail($id);
        $this->modalText = $this->model->name;
    }

    /**
     * Reset delete modal.
     *
     * @return void
     */
    public function resetDeletion(): void
    {
        $this->reset('confirmingDeletion', 'modalText', 'model');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function delete(): void
    {
        $this->model->delete();
        $this->resetDeletion();
        $this->banner(__('The patient has been successfully deleted.'));
    }
}

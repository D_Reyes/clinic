<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Diagnosis;
use Laravel\Jetstream\InteractsWithBanner;
use Rappasoft\LaravelLivewireTables\Views\Filter;

class PatientDiagnosisTable extends DataTableComponent
{
    use InteractsWithBanner;

    /**
     * Indicates if diagnosis deletion is being confirmed.
     *
     * @var bool
     */
    public bool $confirmingDeletion = false;

    /**
     * Show the search field.
     *
     * @var bool
     */
    public bool $showSearch = false;

    /**
     * The current model.
     *
     * @var \App\Models\Diagnosis
     */
    public $model;

    /**
     * The patient.
     *
     * @var \App\Models\Patient
     */
    public $patient;

    /**
     * Current modal text.
     *
     * @var string
     */
    public $modalText;

    public function columns(): array
    {
        return [
            Column::make(__('Date'), 'date')
                ->sortable(),
            Column::blank()
        ];
    }

    /**
     * Define the filters array
     *
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            'start_date' => Filter::make(__('Start date'))
                ->date(),
            'end_date' => Filter::make(__('End date'))
                ->date(),
        ];
    }

    public function query(): Builder
    {
        return Diagnosis::query()
            ->patient($this->patient->id)
            ->when($this->getFilter('start_date'), fn ($query, $search) => $query->startDate($search))
            ->when($this->getFilter('end_date'), fn ($query, $search) => $query->endDate($search));
    }

    public function rowView(): string
    {
        return 'livewire.patient.diagnosis.table.rows';
    }

    /**
     * The view to add any modals for the table, could also be used for any non-visible html
     *
     * @return string
     */
    public function modalsView(): string
    {
        return 'livewire.components.modal.delete';
    }

    /**
     * Show delete modal.
     *
     * @param integer $id
     * @return void
     */
    public function confirmDeletion(int $id): void
    {
        $this->confirmingDeletion = true;
        $this->model = Diagnosis::findOrFail($id);
        $this->modalText = $this->model->name;
    }

    /**
     * Reset delete modal.
     *
     * @return void
     */
    public function resetDeletion(): void
    {
        $this->reset('confirmingDeletion', 'modalText', 'model');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function delete(): void
    {
        $this->model->delete();
        $this->resetDeletion();
        $this->banner(__('The diagnosis has been successfully deleted.'));
    }
}

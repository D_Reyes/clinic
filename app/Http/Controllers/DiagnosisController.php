<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDiagnosisRequest;
use App\Http\Requests\UpdateDiagnosisRequest;
use App\Models\Diagnosis;
use App\Models\Patient;

class DiagnosisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function index(Patient $patient)
    {
        return view('diagnosis.index', compact('patient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        return view('diagnosis.create', compact('patient'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDiagnosisRequest  $request
     * @param  \App\Models\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDiagnosisRequest $request, Patient $patient)
    {
        $data = $request->validated();

        $data['patient_id'] = $patient->id;

        $diagnosis = Diagnosis::create($data);

        session()->flash('flash.banner', __('Diagnosis has been created correctly.'));
        return redirect()->route('patients.diagnoses.show', [$patient->id, $diagnosis->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @param  \App\Models\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient, Diagnosis $diagnosis)
    {
        return view('diagnosis.show', compact('diagnosis', 'patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patient  $patient
     * @param  \App\Models\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient, Diagnosis $diagnosis)
    {
        return view('diagnosis.edit', compact('diagnosis', 'patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDiagnosisRequest  $request
     * @param  \App\Models\Patient  $patient
     * @param  \App\Models\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDiagnosisRequest $request, Patient $patient, Diagnosis $diagnosis)
    {
        $data = $request->validated();

        $diagnosis->update($data);

        session()->flash('flash.banner', __('Diagnosis has been edited successfully.'));
        return redirect()->route('patients.diagnoses.show', [$patient->id, $diagnosis->id]);
    }
}

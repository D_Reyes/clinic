<?php

namespace Tests\Feature;

use App\Http\Livewire\UserTable;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * The user model.
     *
     * @var \App\Models\User
     */
    protected User $user;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(RoleSeeder::class);
        $this->user = User::factory()->admin()->create();
    }

    /** @test  */
    public function test_a_user_can_read_all_the_users()
    {
        $this->actingAs($this->user);

        User::factory()->create();

        $response = $this->get('/users');

        $response->assertStatus(200);
    }

    /** @test  */
    public function test_a_user_can_read_single_user()
    {
        $this->actingAs($this->user);

        $user = User::factory()->create();

        $response = $this->get('/users/' . $user->id);

        $response->assertSee('200');
    }

    /** @test  */
    public function test_authenticated_users_can_create_a_new_user()
    {
        $this->actingAs($this->user);

        $user = User::factory()->make();
        $user->makeVisible(['password']);
        $user->password_confirmation = $user->password;

        $response = $this->post('/users', $user->toArray());

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
    }

    /** @test  */
    public function test_unauthenticated_users_cannot_create_a_new_user()
    {
        $user = User::factory()->make();

        $response = $this->post('/users', $user->toArray());

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function test_authenticated_users_can_update_the_user()
    {
        $this->actingAs($this->user);

        $user = User::factory()->create();
        $user->name = '--TEST--';

        $this->put('/users/' . $user->id, $user->toArray());

        $this->assertEquals('--TEST--', $user->fresh()->name);
    }

    /** @test  */
    public function test_unauthenticated_users_cannot_update_the_user()
    {
        $user = User::factory()->create();
        $user->name = '--TEST--';

        $response = $this->put('/users/' . $user->id, $user->toArray());

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function test_users_can_delete_the_user()
    {
        $this->actingAs($this->user);

        $user = User::factory()->create();

        Livewire::test(UserTable::class)
                ->set('model', $user)
                ->call('delete');

        $this->assertNotNull($user->fresh()->deleted_at);
    }
}

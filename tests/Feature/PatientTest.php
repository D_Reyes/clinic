<?php

namespace Tests\Feature;

use App\Http\Livewire\PatientTable;
use App\Models\Patient;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class PatientTest extends TestCase
{
    use RefreshDatabase;

    /**
     * The user model
     *
     * @var \App\Models\User
     */
    protected User $user;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(RoleSeeder::class);
        $this->user = User::factory()->create();
    }

    /** @test  */
    public function test_a_user_can_read_all_the_patients()
    {
        $this->actingAs($this->user);

        Patient::factory()->create();

        $response = $this->get('/patients');

        $response->assertStatus(200);
    }

    /** @test  */
    public function test_a_user_can_read_single_patient()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();

        $response = $this->get('/patients/' . $patient->id);

        $response->assertStatus(200);
    }

    /** @test  */
    public function test_authenticated_users_can_create_a_new_patient()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->make();

        $response = $this->post('/patients', $patient->toArray());

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
    }

    /** @test  */
    public function test_unauthenticated_users_cannot_create_a_new_patient()
    {
        $patient = Patient::factory()->make();

        $response = $this->post('/patients', $patient->toArray());

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function test_authenticated_users_can_update_the_patient()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();
        $patient->name = '--TEST--';

        $this->put('/patients/' . $patient->id, $patient->toArray());

        $this->assertEquals('--TEST--', $patient->fresh()->name);
    }

    /** @test  */
    public function test_unauthenticated_users_cannot_update_the_patient()
    {
        $patient = Patient::factory()->create();
        $patient->name = '--TEST--';

        $response = $this->put('/patients/' . $patient->id, $patient->toArray());

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function test_users_can_delete_the_patient()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();

        Livewire::test(PatientTable::class)
                ->set('model', $patient)
                ->call('delete');

        $this->assertNotNull($patient->fresh()->deleted_at);
    }
}

<?php

namespace Tests\Feature;

use App\Http\Livewire\PatientDiagnosisTable;
use App\Models\Diagnosis;
use App\Models\Patient;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class DiagnosisTest extends TestCase
{
    use RefreshDatabase;

    /**
     * The user model
     *
     * @var \App\Models\User
     */
    protected User $user;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->seed(RoleSeeder::class);
        $this->user = User::factory()->create();
    }

    /** @test  */
    public function test_a_user_can_read_all_the_diagnosis()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();
        Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->create();

        $response = $this->get("/patients/{$patient->id}/diagnoses");

        $response->assertStatus(200);
    }

    /** @test  */
    public function test_a_user_can_read_single_diagnosis()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();
        $diagnosis = Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->create();

        $response = $this->get("/patients/{$patient->id}/diagnoses/{$diagnosis->id}");

        $response->assertStatus(200);
    }

    /** @test  */
    public function test_authenticated_users_can_create_a_new_diagnosis()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();
        $diagnosis = Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->make();

        $response = $this->post("/patients/{$patient->id}/diagnoses", $diagnosis->toArray());

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
    }

    /** @test  */
    public function test_unauthenticated_users_cannot_create_a_new_diagnosis()
    {
        $patient = Patient::factory()->create();
        $diagnosis = Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->make();

        $response = $this->post("/patients/{$patient->id}/diagnoses", $diagnosis->toArray());

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function test_authenticated_users_can_update_the_diagnosis()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();
        $diagnosis = Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->create();

        $diagnosis->description = '--TEST--';

        $this->put("/patients/{$patient->id}/diagnoses/{$diagnosis->id}", $diagnosis->toArray());

        $this->assertEquals('--TEST--', $diagnosis->fresh()->description);
    }

    /** @test  */
    public function test_unauthenticated_users_cannot_update_the_diagnosis()
    {
        $patient = Patient::factory()->create();
        $diagnosis = Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->create();

        $diagnosis->description = '--TEST--';

        $response = $this->put("/patients/{$patient->id}/diagnoses/{$diagnosis->id}", $diagnosis->toArray());

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function test_users_can_delete_the_diagnosis()
    {
        $this->actingAs($this->user);

        $patient = Patient::factory()->create();
        $diagnosis = Diagnosis::factory()->state([
            'patient_id' => $patient->id,
        ])->create();

        Livewire::test(PatientDiagnosisTable::class, ['patient' => $patient])
                ->set('model', $diagnosis)
                ->call('delete');

        $this->assertNotNull($diagnosis->fresh()->deleted_at);
    }
}

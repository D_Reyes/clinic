'use strict';

$(function () {

    moment.locale('es');

    $('[data-datetimepicker="datetime"]').flatpickr({
        locale: 'es',
        altInput: true,
        altFormat: 'd/m/Y H:i',
        enableTime: true,
        dateFormat: 'Y-m-d H:i',
    });

    $('[data-datetimepicker="date"]').flatpickr({
        locale: 'es',
        altInput: true,
        altFormat: 'd/m/Y',
        enableTime: true,
        dateFormat: 'Y-m-d',
    });

});

<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'name' => 'Admin',
                'email' => 'admin@demo.test',
                'password' => Hash::make('admin'),
                'email_verified_at' => now(),
                'role_id' => 1, // Admin
            ],
            [
                'name' => 'User',
                'email' => 'user@demo.test',
                'password' => Hash::make('user'),
                'email_verified_at' => now(),
                'role_id' => 2, // User
            ],
        ]);

        if ('production' !== config('app.env')) {
            User::factory()
                ->count(10)
                ->state(new Sequence(
                    fn ($sequence) => ['role_id' => Role::all()->random()],
                ))
                ->create();
        }
    }
}

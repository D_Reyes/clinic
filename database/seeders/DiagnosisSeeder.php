<?php

namespace Database\Seeders;

use App\Models\Diagnosis;
use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DiagnosisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Diagnosis::factory(200)
            ->state(new Sequence(
                fn ($sequence) => ['patient_id' => Patient::limit(30)->get()->random()],
            ))
            ->create();
    }
}

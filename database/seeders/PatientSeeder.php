<?php

namespace Database\Seeders;

use App\Models\Diagnosis;
use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Patient::factory(50)
            ->has(
                Diagnosis::factory(rand(2, 5))
            )
            ->create();
    }
}

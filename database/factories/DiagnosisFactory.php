<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiagnosisFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->dateTimeThisYear(),
            'description' => implode('', array_map(function ($item) {
                return "{$item}";
            }, $this->faker->paragraphs(rand(2, 5)))),
            'patient_id' => Patient::factory(),
        ];
    }
}

<?php

use App\Models\Diagnosis;
use App\Models\Patient;
use App\Models\User;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator;

// Dashboard
Breadcrumbs::for('dashboard', function (Generator $trail) {
    $trail->push(__('Dashboard'), route('dashboard'));
});

// Profile
Breadcrumbs::for('profile.show', function (Generator $trail) {
    $trail->push(__('Profile'), route('profile.show'));
});

// Users
Breadcrumbs::for('users.index', function (Generator $trail) {
    $trail->parent('dashboard');
    $trail->push(__('Users'), route('users.index'));
});

Breadcrumbs::for('users.create', function (Generator $trail) {
    $trail->parent('users.index');
    $trail->push(__('New User'), route('users.create'));
});

Breadcrumbs::for('users.show', function (Generator $trail, User $user) {
    $trail->parent('users.index');
    $trail->push($user->name, route('users.show', $user));
});

Breadcrumbs::for('users.edit', function (Generator $trail, User $user) {
    $trail->parent('users.show', $user);
    $trail->push(__('Edit User'), route('users.edit', $user));
});

// Patients
Breadcrumbs::for('patients.index', function (Generator $trail) {
    $trail->parent('dashboard');
    $trail->push(__('Patients'), route('patients.index'));
});

Breadcrumbs::for('patients.create', function (Generator $trail) {
    $trail->parent('patients.index');
    $trail->push(__('New Patient'), route('patients.create'));
});

Breadcrumbs::for('patients.show', function (Generator $trail, Patient $patient) {
    $trail->parent('patients.index');
    $trail->push($patient->full_name, route('patients.show', $patient));
});

Breadcrumbs::for('patients.edit', function (Generator $trail, Patient $patient) {
    $trail->parent('patients.show', $patient);
    $trail->push(__('Edit Patient'), route('patients.edit', $patient));
});

// Diagnoses
Breadcrumbs::for('patients.diagnoses.index', function (Generator $trail, Patient $patient) {
    $trail->parent('patients.show', $patient);
    $trail->push(__('Diagnoses'), route('patients.diagnoses.index', $patient));
});

Breadcrumbs::for('patients.diagnoses.create', function (Generator $trail, Patient $patient) {
    $trail->parent('patients.diagnoses.index', $patient);
    $trail->push(__('New Diagnosis'), route('patients.diagnoses.create', $patient));
});

Breadcrumbs::for('patients.diagnoses.show', function (Generator $trail, Patient $patient, Diagnosis $diagnosis) {
    $trail->parent('patients.diagnoses.index', $patient);
    $trail->push($diagnosis->id . ' - ' . $diagnosis->date->isoFormat('L'), route('patients.diagnoses.show', [
        $patient,
        $diagnosis,
    ]));
});

Breadcrumbs::for('patients.diagnoses.edit', function (Generator $trail, Patient $patient, Diagnosis $diagnosis) {
    $trail->parent('patients.diagnoses.show', $patient, $diagnosis);
    $trail->push(__('Edit Diagnosis'), route('patients.diagnoses.edit', [$patient, $diagnosis]));
});

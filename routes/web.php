<?php

use App\Http\Controllers\DiagnosisController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    // Dashboard
    Route::get('/', fn () => view('dashboard'))->name('dashboard');

    // Patients
    Route::resource('/patients', PatientController::class)->except('destroy');
    Route::group([
        'prefix' => '/patients/{patient}',
        'as' => 'patients.',
    ], function () {
        // Diagnoses
        Route::resource('/diagnoses', DiagnosisController::class)->except('destroy');
    });

    Route::middleware('isAdmin')->group(function () {
        // Users
        Route::resource('/users', UserController::class)->except('destroy');
    });
});

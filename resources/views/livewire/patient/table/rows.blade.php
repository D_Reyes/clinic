<x-livewire-tables::table.cell>
    <div class="text-sm font-medium text-gray-900">
        {{ $row->full_name }}
    </div>

    <div class="text-sm text-gray-500">
        {{ $row->id_card }}
    </div>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell class="hidden md:table-cell">
    {{ $row->email }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell class="hidden md:table-cell">
    {{ $row->phone }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell class="hidden md:table-cell">
    {{ $row->city }}
</x-livewire-tables::table.cell>

@if (!$columnSelect || ($columnSelect && $this->isColumnSelectEnabled('country')))
    <x-livewire-tables::table.cell class="hidden md:table-cell">
        {{ $row->country }}
    </x-livewire-tables::table.cell>
@endif

<x-livewire-tables::table.cell>
    <div class="text-right">
        <a href="{{ route('patients.show', $row->id) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">{{ __('View') }}</a>
        <a href="{{ route('patients.edit', $row->id) }}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">{{ __('Edit') }}</a>
        <a href="#" wire:click.prevent="confirmDeletion({{ $row->id }})" class="text-red-600 hover:text-red-900 mb-2 mr-2">{{ __('Delete') }}</a>
    </div>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->date->isoFormat('L') }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <div class="text-right">
        <a href="{{ route('patients.diagnoses.show', [$patient->id, $row->id]) }}" class="text-blue-600 hover:text-blue-900 mb-2 mr-2">{{ __('View') }}</a>
        <a href="{{ route('patients.diagnoses.edit', [$patient->id, $row->id]) }}" class="text-indigo-600 hover:text-indigo-900 mb-2 mr-2">{{ __('Edit') }}</a>
        <a href="#" wire:click.prevent="confirmDeletion({{ $row->id }})" class="text-red-600 hover:text-red-900 mb-2 mr-2">{{ __('Delete') }}</a>
    </div>
</x-livewire-tables::table.cell>

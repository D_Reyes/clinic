<x-jet-dialog-modal wire:model="confirmingDeletion">
    <x-slot name="title">
        {{ __('Delete item') }}
    </x-slot>

    <x-slot name="content">
        {{ __('Are you sure you want to delete the item :name?', [ 'name' => $modalText ]) }}
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="resetDeletion" wire:loading.attr="disabled">
            {{ __('Cancel') }}
        </x-jet-secondary-button>

        <x-jet-danger-button class="ml-3" wire:click="delete" wire:loading.attr="disabled">
            {{ __('Delete') }}
        </x-jet-danger-button>
    </x-slot>
</x-jet-dialog-modal>

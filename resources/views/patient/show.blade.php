<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('View Patient') }}
        </h2>
    </x-slot>

    <x-slot name="actions">
        <a href="{{ route('patients.diagnoses.index', $patient->id) }}" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition mr-3">{{ __('View Diagnoses') }}</a>
        <a href="{{ route('patients.edit', $patient->id) }}" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">{{ __('Edit Patient') }}</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">{{ __('Patient Information') }}</h3>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <form action="#" method="POST">
                            <div class="shadow overflow-hidden sm:rounded-md bg-gray-200">
                                <div class="px-4 py-5 bg-white sm:p-6">
                                    <div class="grid grid-cols-6 gap-6">
                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="name" class="block text-sm font-medium text-gray-700">{{ __('Name') }}</label>
                                            <input type="text" name="name" id="name" value="{{ $patient->name }}" readonly autocomplete="given-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="surname" class="block text-sm font-medium text-gray-700">{{ __('Surname') }}</label>
                                            <input type="text" name="surname" id="surname" value="{{ $patient->surname }}" readonly autocomplete="family-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-2 lg:col-span-2">
                                            <label for="id_card" class="block text-sm font-medium text-gray-700">{{ __('ID card') }}</label>
                                            <input type="text" name="id_card" id="id_card" value="{{ $patient->id_card }}" readonly autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-2 lg:col-span-2">
                                            <label for="phone" class="block text-sm font-medium text-gray-700">{{ __('Phone') }}</label>
                                            <input type="text" name="phone" id="phone" value="{{ $patient->phone }}" readonly autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-4">
                                            <label for="email" class="block text-sm font-medium text-gray-700">{{ __('Email address') }}</label>
                                            <input type="text" name="email" id="email" value="{{ $patient->email }}" readonly autocomplete="email" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="country" class="block text-sm font-medium text-gray-700">{{ __('Country') }}</label>
                                            <input type="text" name="country" id="country" value="{{ $patient->country }}" readonly autocomplete="email" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6">
                                            <label for="address" class="block text-sm font-medium text-gray-700">{{ __('Street address') }}</label>
                                            <input type="text" name="address" id="address" value="{{ $patient->address }}" readonly autocomplete="address" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                                            <label for="city" class="block text-sm font-medium text-gray-700">{{ __('City') }}</label>
                                            <input type="text" name="city" id="city" readonly autocomplete="city" value="{{ $patient->city }}" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label for="state" class="block text-sm font-medium text-gray-700">{{ __('State / Province') }}</label>
                                            <input type="text" name="state" id="state" value="{{ $patient->state }}" readonly autocomplete="state" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                            <label for="postcode" class="block text-sm font-medium text-gray-700">{{ __('ZIP / Postal code') }}</label>
                                            <input type="text" name="postcode" id="postcode" value="{{ $patient->postcode }}" readonly autocomplete="postcode" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

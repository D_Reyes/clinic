<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('View Diagnosis') }}
        </h2>
    </x-slot>

    <x-slot name="actions">
        <a href="{{ route('patients.diagnoses.edit', [$patient->id, $diagnosis->id]) }}" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition">{{ __('Edit Diagnosis') }}</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-10 sm:mt-0">
                <div class="md:grid md:grid-cols-3 md:gap-6">
                    <div class="md:col-span-1">
                        <div class="px-4 sm:px-0">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">{{ __('Diagnosis Information') }}</h3>
                        </div>
                    </div>
                    <div class="mt-5 md:mt-0 md:col-span-2">
                        <form action="#" method="POST">
                            <div class="shadow overflow-hidden sm:rounded-md bg-gray-200">
                                <div class="px-4 py-5 bg-white sm:p-6">
                                    <div class="grid grid-cols-6 gap-6">
                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="date" class="block text-sm font-medium text-gray-700">{{ __('Date') }}</label>
                                            <input type="text" name="date" id="date" value="{{ $diagnosis->date->isoFormat('L H:m') }}" readonly autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6 sm:col-span-3">
                                            <label for="dignosis[patient][full_name]" class="block text-sm font-medium text-gray-700">{{ __('Patient') }}</label>
                                            <input type="text" name="dignosis[patient][full_name]" id="dignosis[patient][full_name]" value="{{ $patient->full_name }}" readonly autocomplete="given-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200">
                                        </div>

                                        <div class="col-span-6">
                                            <label for="description" class="block text-sm font-medium text-gray-700">{{ __('Description') }}</label>
                                            <textarea id="description" name="description" readonly rows="20" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md bg-gray-200" placeholder="{{ __('Diagnosis...') }} ">{{ $diagnosis->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
